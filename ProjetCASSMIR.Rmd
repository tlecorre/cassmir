---
title: "Le projet et les sources"
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```


Suite à un travail d'harmonisation et d'agrégation de données individuelles, la base CASSMIR (Contribution à l'Analyse Spatiale et Sociale des Marchés Immobiliers Résidentiels) donne accès à la communauté scientifique des jeux de données pour saisir les configurations spatiales et sociales du marché immobilier. Cette rubrique vise à présenter la démarche de la production de la base de données CASSMIR et à documenter les sources mobilisées.

## Conceptualisation de la base de données CASSMIR

Un enjeu scientifique demeure important dans l'analyse des marchés immobiliers en France. En effet, si les données brutes existent, y accéder est difficile, pénalisant une recherche transparente et reproductible.
La base de données CASSMIR répond à cet enjeu pour la discipline géographique, et plus largement pour les sciences sociales. Réalisée dans le cadre de l'["ANR WIsDHoM"](https://anr.fr/fr/projets-finances-et-impact/projets-finances/projet/funded/project/anr-18-ce41-0004/?tx_anrprojects_funded%5Bcontroller%5D=Funded&cHash=99ff273efab1cb69decf22e2eb2cc798), les données de la base CASSMIR ont été appariées dans l'objectif de mesurer les inégalités, sur et par le marché immobilier. Toutefois les données ne sont pas restrictives à cette thématique. A l'état actuel, la première version de la base CASSMIR est une base de données entièrement construites à partir d'informations sur les marchés immobiliers résidentiels en partant des transactions immobilières d'Île-de-France (issues de la base BIEN) et des acquisitions de primo-accédants ayant bénéficié d'un Prêt à Taux Zéro (issues de la base PTZ). 

### Structure de la base de données CASSMIR

Cette base se décline en deux branches. Une première branche propose un livrable en construisant des indicateurs agrégés à partir d'une référence par unités spatiales, et ceci à plusieurs échelles. Une seconde branche propose un livrable d'indicateurs agrégés à partir d'une référence sur des groupes de population 

Chaque groupe de population ou unité spatiale est dupliqué pour chaque période annuelle d'agrégation. Le nombre de variables entre lesjeux de données de la même branche peut évoluer en fonction de la présence et de la granularité des informations non-agrégées. 


### Le contenu des données CASSMIR : les quatre grands champs d'investigation

Dans ce projet, aucun travail de collecte, ni de production *ex nihilo* n'a été réalisé. Il s'agit essentiellement d'un excercice de préparation de deux jeux de données brutes attributaires, avec :

- La base BIEN produite par la Chambre des notaires

- La base PTZ produite par le SGFGAS (Société de Gestion des Financements et de la Garantie de l’Accession Sociale à la propriété)


Ces deux sources permettent de produire des indicateurs harmonisés qui ont été rassemblés dans quatre grands champs distincts :

- Les indicateurs sur les prix 

- Les indicateurs sur le profil des acquéreurs-vendeurs

- Les indicateurs sur les régimes d'achat et de mutation du bien

- Les indicateurs sur les types de biens immobiliers concernés par les transactions

La production des indicateurs fait suite à plusieurs étapes de préparation et de consolidation des données sources. 

## Présentation des sources 

Deux sources principales sont donc mobilisées pour livrer des informations spatialement agrégées sur les dynamiques des marchés immobiliers. La première est la Base d'Informations Economiques Notariales (BIEN), produite paar la Chambre des notaires. La seconde est la base Prêts à Taux Zéro (PTZ), produite par la Société de Gestion des Financements et de la Garantie de l’Accession Sociale à la propriété (SGFGAS). 

Ces deux bases de données, différentes dans leur nature, sont complémentaires dans le projet. La base BIEN restitue des informations concernant la transaction immobilière, située dans l'espace et dans le temps. Quant à la base PTZ, elle restitue des informations sur les achats contractés avec un des prêts immobiliers à taux zéro (PTZ). En d'autres termes, la population de la base BIEN est l'ensemble des transactions immobilières, celle de la base PTZ, une opération financière réalisée dans le cadre d'une transaction immobilière par un investissement spécifique (uniquement les investissements avec PTZ). Sans avoir procédé à des recoupements sytématiques entre les deux bases à un niveau individuel, la non-exhaustivité de la base BIEN et le poids des opérations PTZ dans l'ensemble des transactions immobilières, empêche de travailler sur une même base consolidée à un niveau individuel. La base BIEN vise à renvoyer une image générale du marché. La base PTZ est principalement utilisée pour étudier les dispositifs d'accession à la propriété mais également pour compléter des informations non disponibles dans BIEN, en particulier sur les conditions d'investissement par crédit. 

La préparation des données brutes vers livrable formalisé en une base accessible concerne principalement la base de données BIEN. Cette source est la plus complexe, la plus difficile à documenter et à traiter, n'est pas autorisée à être diffusée en libre-accès  et constitue en même temps la principale référence de nos analyses. Pour ces raisons, cette source demande une attention particulière. 

### Base BIEN

Le jeu de données brutes original est un échantillon de transactions immobilières géolocalisées (en grande partie) de 1996, 1999, 2003 à 2012 extrait de la base de données BIEN produite par la chambre des Notaires, gérée et commercialisée par l'association Paris Notaires Services (PNS).

#### Historique et présentation générale 

La base BIEN a été créée en version papier dans les années 1970 par quelques notaires parisiens. Il faut attendre 1991 pour passer vers une base numérisée centralisée à l'initiative de la Chambre des Notaires de Paris et de l'Île-de-France. La base a été produite dans l'objectif de renforcer la connaissance des notaires sur les marchés immobiliers. Réservée dans un premier temps au périmètre de la commune de Paris et aux départements de la première couronne, la collecte généralisée de données à l'ensemble des études notariales franciliennes remonte à 1996. Acteurs juridiques indispensables dans les transactions immobilières, les notaires ont donné une importance à la valorisation des données de marchés dans une période où elles étaient encore peu étayées et décentralisées.

La collecte de données a lieu lors de la mutation du bien (changement du propriétaire). Traditionnellement, les informations de la transaction sont envoyées en version papier par courrier postal au service de gestion et de développement des bases de données, l'association Paris Notaires Services (PNS) pour la base BIEN, qui se charge d'alimenter la base.
Des années 1990 jusqu'à une période récente, l'alimentation de la base de données était largement soumise au bon vouloir des notaires. Certains d'entre-eux ont donc pu être réfractaires en raison du coût de l'acheminent des informations pour contribuer à l'alimentation de la base. Depuis 2010, les informations sont transmises directement par les offices en télé-transmission minimisant les contraintes d'envoi, le délai de recensement et l'intermédiation d'alimentation de la base.

En France, en dehors des données DVF, aucune base de données immobilières et foncières exploitable ne prétend être exhaustive. Pour la base BIEN, la collecte des données s'est sensiblement améliorée au fil du temps grâce à l'encouragement de la chambre des notaires, auprès de ses membres, pour constituer une telle source informationnelle sur les marchés. Ceci va de pair avec le rôle accru des notaires dans les services de conseils et d'expertises du marché immobilier. Les services de gestion des données estiment que la base BIEN est représentative de 70 à 80% du marché réel des transactions immobilières et foncières en Île-de-France. Les départements de première couronne affichent plus de 80% du total des transactions du marché de gré à gré renseignées dans la base BIEN. Pour le reste, nous sommes entre 60 et 80%. De ce fait, malgré des modalités de recensement parfois un peu flottantes, mais stabilisées récemment, cette base de données offre une bonne représentativité du marché francilien.

La base BIEN produite par les notaires recensent les informations des transactions pour huit types de biens distincts : les maisons individuelles, les appartements, les terrains, les garages, les immeubles entiers, les locaux d'activités (bureaux, entrepôts, ateliers, commerces…), les biens viticoles et les biens agricoles. Il existe un socle commun de variables renseignées auquel s'ajoute des variables spécifiques par types de biens. Au total, c'est plus de 130 variables qui apparaissent dans la base de données.

Aujourd'hui la base BIEN est commercialisée auprès des acteurs privés et publics. Un [site internet](https://basebien.com/?fsid=1) y est totalement consacré à cette fin.

#### Echantillon de la base BIEN réceptionné et modalités d'utilisation

L'échantillon réceptionné, qui se présente sous une forme de tableau de renseignement élémentaire, recense les transactions de 1996 (année de la collecte des données à l'ensemble du territoire francilien) à 2012. Chaque ligne de l'échantillon représente une transaction. L'échantillon a été composé en deux temps. Une partie a d'abord été réceptionnée en 2015, composée de 545 879 transactions pour les années 1996, 1999, 2003, 2006, 2008, 2010 et 2012. En 2016, un second acheminement a été réalisée avec 422 816 transactions pour les années 2004, 2005, 2007, 2009, 2011. 

L'échantillon de la base de données Base d'Informations Economiques Notariales (BIEN) a été mis à disposition par [Paris Notaire Service (PNS)](http://paris-notaires-services.fr/), au nom de la Chambre des Notaires de Paris et d'Île-de-France, en vertu d'un contrat conclu par le LabEx DynamiTe (ANR-11-LABX-0046) et l'Université Paris 1 Panthéon-Sorbonne. Paris Notaire Service est un fournisseur de données commerciales, et n'autorise pas les chercheurs à partager publiquement des données fournies à des fins de recherche. Cependant, les ensembles de données utilisés pour cette étude peuvent être obtenus par tout autre chercheur, par l'intermédiaire du même fournisseur, moyennant des frais.

Les données issues de la base BIEN utilisées dans ce travail sont issues de deux livrables de Paris Notaire Service (PNS). Il s'agit d'un échantillon, complété en 2016, avec 50% des transactions de la base initiale dans les communes de plus de 10 000 habitants en 2011 et 100% des transactions de la base initiale dans les communes de moins de 10 000 habitants en 2011. 
**L'accès à ces données est strictement réservé aux membres signataires de la convention. Les données ne peuvent donc pas être partagées**. 

Des conditions d'utilisation et de diffusion des résultats produits sont imposées dans la convention :

"Le Client garantit queles Résultats ne constituent d’aucune  manière  des  données à caractère personnel (qu’elles soient directement ou indirectement nominatives) au sens de la loi n°78-17 du 6 janvier 1978 et que ce sont des données statistiques anonymes au sens de la doctrine de la Commission Nationale de l’Informatique et des Libertés. Ainsi, le Client s’interdit de produire des agrégats et de sélectionner des zones géographiques permettant  d’identifier des transactions réelles. Le Client est autorisé à utiliser les Données pour construire des Résultats sur la base de 5 références minimum. Le Client s’interdit en particulier de produire un prix à l’adresse dans les zones d’habitat individuel à partir des données de la Base Bien ou de l’Extrait de la Base Bien et le cas échéant, de ses Mises à jour." 


#### Informations techniques et informations sur les données

L'échantillon réceptionné est une ressource volumineuse en un seul fichier en format.txt encodé en utf-8 de 424Mo qui répertorie 968 695 transactions immobilières --- pas de foncier --- (en ligne) pour 96 variables (colonnes). Ces données sont (théoriquement) géolocalisées à l'adresse du bien immobilier avec coordonnées X,Y en lambert 2 étendu. Comme il a été dit précédemment, les données BIEN recouvrent uniquement les communes de la région administrative parisienne. 


L'échantillon réceptionné concerne uniquement des transactions en gré à gré de logements (biens immobiliers à usage d'habitation) pour deux types de biens : les appartements et les maisons. Chaque ligne de la base correspond à un bien échangé, c'est-à-dire une transaction avec son propre identifiant. Chaque transaction est caractérisée par 95 variables distinctes.

Le lecteur est invité à prendre connaissance du dictionnaire de la base de données. Il s'agit de l'unique document proposé par les services de la Chambre des notaires pour accompagner la base de données. Il n'y a pas à ce jour de documents de métadonnées de la base BIEN. Toutefois, les données sur les prix et les caractéristiques des logements sont validées par l'Insee, qui les utilise dans le cadre de la production des [indices Notaires-Insee](https://www.insee.fr/fr/metadonnees/source/indicateur/p1643/description) et les [données produites conjointement par l’Insee et les notaires sont labellisées par l’Autorité de la statistique publique](https://www.insee.fr/fr/metadonnees/source/indicateur/p1643/mandat-institutionnel). Dans ce cadre, L'Insee propose une documentation relative à la construction des indices de prix et livre plusieurs éléments informatifs sur la base de données BIEN (et son équivalent PERVAL pour les autres régions administratives françaises), disponibles [ici](https://www.insee.fr/fr/information/4175280). Les séries d'indices sont diffusées par l'Insee, mises à disposition dans sa banque de données macro-économiques (BDM). Elles peuvent être visualisées et téléchargées [ici](https://www.insee.fr/fr/statistiques/series/105071770).

Celle-ci se présente sous un format de tableau de données structuré avec, en ligne, l'identifiant de chaque transaction et, en colonne, les variables qui caractérisent les transactions. Chaque variable participe à la caractérisation des transactions immobilières désagrégées avec une série d'informations élémentaires, géographiques et temporelles. 

Les variables sont de différentes natures, à la fois quantitatives et qualitatives. Nous nous arrêtons ici à une description générale des variables, regroupées en cinq grandes familles, présentent dans l'échantillon brut pour en faciliter la lecture : 

- Variables sur la localisation du bien, permettant de localiser le bien, du niveau départemental à la géolocalisation à l’adresse en X,Y.

- Variables financières, comprenant des informations sur les montants de la transaction, le prix au $m^2$, des inforamtions fiscales, des informations sur le crédit, des informations sur la plus ou moins-value.

- Variables descriptives sur la qualité du bien immobilier.  Certaines variables ont un intérêt : type de bien, surface, ancienneté, nombre de pièces. D'autres sont assez superflues pour ce travail : présence d'un anneau d'amarrage pour bateau, niveau de l'emplacement du garage ou du parking).

- Variables temporelles sur la mutation du bien et la mutation précédente du bien.

- Variables qui permettent de caractériser le profil des acquéreurs et des vendeurs : CSP (en 24 postes mais renseignés de manière hétérogène) ou qualité, âge, sexe, situation matrimoniale, nationalité, commune d'origine, année de naissance. 
	
*Un dictionnaire des variables de la base BIEN est disponible [ici](https://gitlab.huma-num.fr/tlecorre/cassmir/-/blob/master/2015-01-08_Dictionnaire_des_variables_BIEN.xls)*
	
Les informations contenues dans la base sont donc nombreuses. Toutefois, le coût pour y accéder reste financièrement très élevé. 

### Base PTZ

Les données du SGFGAS (Société de Gestion des Financements et de la Garantie de l’Accession Sociale à la propriété) permettent de faire face à certaines informations lacunaires des données sur les transactions immobilières pour étudier le crédit immobilier.

#### Historique et présentation générale 

Les données de la base PTZ sont issues d'un recensement exhaustif sur les opérations financées par des prêts à taux zéro (PTZ) depuis 1995 en France. Les organismes de crédit ont la charge de transmettre au SGFGAS les informations sur les opérations (achats d'un bien immobilier) financées avec un prêt complémentaire PTZ. En effet, ces données, "obtenues dans le cadre de la distribution de ces prêts subventionnés par l’État, sont transmises par les organismes de crédits qui le distribuent dans le cadre d’une convention avec l’État, et la SGFGAS (Société de Gestion des Financements et de la Garantie de l’Accession Sociale à la propriété)." [voir source](https://www.data.gouv.fr/fr/datasets/base-de-donnees-ptz-prets-a-taux-zero/)

#### Base PTZ réceptionnée et modalités d'utilisation

Les données de la base PTZ ont été partiellement ouvertes au public et sont aujourd'hui disponibles [en ligne](https://www.data.gouv.fr/fr/datasets/base-de-donnees-ptz-prets-a-taux-zero/). Dans notre cas, nous avons réceptionné en 2016 la base de données complète dans le cadre d'une convention avec le SGFGAS.

La base de données du fichier Prêts à Taux Zéro (PTZ) a été gracieusement mise à disposition par la Société de Gestion des Financements et de la Garantie de l'Accession Sociale à la propriété (SGFGAS), en vertu d'un accord conclu avec L'Unité Mixte de Recherche Géographie-Cités (UMR 8504) et Centre National de la Recherche Scientifique (CNRS). Ces données sont soumises à des conditions d'utilisation et de diffusion qui n'autorisent pas les chercheurs à les partager publiquement.

#### Informations techniques et informations sur les données

La base de données est très volumineuse : 1.375 Gb. Il s'agit d'un fichier en format.txt encodé en utf-8. Il s'agit d'une base de données très riches (162 variables pour plus de 3 millions de lignes --- recouvrant toute la France) et de bonnes qualités. Il n'y a pas de document de métadonnées qui accomagne la base. Toutefois, un dictionnaire de la base est diponible (format word), lequel précise certaines conditions d'utilisation et d'exploitation des données et livre également des informations sur la construction des variables. 

Outre les informations sur les prêts complémentaires, les données renseignent également les modalités des prêts principaux pour le financement d'une opération : montant du (des) crédit(s) principal, taux d'intérêts, durée du crédit principal, montant des mensualités, etc. Dans notre cas, ces informations sont précieuses pour disposer des informations souvent très difficiles à obtenir sur les prêts bancaires privés.

Ces informations sur le prêt principal s'accompagnent d'autres renseignements. De nombreuses caractéristiques de l'emprunteur sont détaillées : âge, catégorie socio-professionnelle d'appartenance, revenus,  composition familiale ; des informations sur les opérations : prix d'achat total du bien, type d'opération, etc. ; enfin, des informations de localisation de l'opération à la commune permettent de mener un travail géographique. La seule limite de ces données est qu'elle recouvre une étendue limitée de l'ensemble des prêts immobiliers accordés et des types d'investissement immobiliers (essentiellement les prêts pour des primo-accédants éligibles).



## Livrable par références spatiales

Le livrable par références spatiales se décline en trois sorties. Une première à l'échelle communale, une seconde à l'échelle des carreaux de 1km de côté et une troisième à l'échelle des carreaux de 200m de côté. L'objectif est de produire des séries d'indicateurs qui caractérisent les unités spatiales, par des données contextuelles socio-économiques et surtout par des données sur les dynamiques des marchés immobiliers.


Si la majorité des informations se retrouvent dans les trois sorties, il existe une disparité entre elles. En effet, pour des raisons de contraintes d'utilisation, de disponibilité de l'information géographique renseignée dans les sources d'origine et de choix méthodologiques, les variables de sortie du livrable ne se recoupent pas totalement entre la sortie à l'échelle communale et les deux sorties à l'échelle des carreaux 1km et 200m de côté. Par exemple, certaines données ne permettent de délivrer qu'une information à l'échelle communale. Par ailleurs, des unités spatiales à un niveau fin peut disposer d'informations alors même que sa maille supérieure d'appartenance ne dipose d'aucune information. Il s'agit dans ce cas d'un choix d'interpolation des données d'origine désagrégées sur une grille régulière fine qui limite la perte d'information.

Malgré le non recoupement total des informations entre les échelles des sorties, toutes les opérations sont réalisées sur le principe d'étudier trois thémes principaux : les dynamiques spatiales des prix, les dynamiques socio-spatiales et la géographie des produits immobiliers.


#### Champ d'investigation sur les prix immobiliers

- Source : échantillon BIEN 

- Informations pincipales : Prix nominal net vendeur, prix au $m^2$

- Types de données de sortie : variables quantitatives continues

- Variables d'origine : REQ_PRIX, REQ_PM2, REQTYPBIEN, NBPIECES

- Niveau le plus fin d'agrégation  en sortie : carreaux 200m


#### Champ d'investigation sur le profil des acquéreurs-vendeurs

- Source : échantillon BIEN 

- Informations pincipales : Profil des acquéreurs et vendeurs par type (particulier, personne morale...), Catégories socio-professionnelles, âge, sexe, situation matrimoniale, statut d'activité (actif, retraité, inactif), origine géographique de l'acquéreur

- Types de données de sortie : variables quantitatives continues (exprimées en %)

- Variables d'origine : QUALITE_AC, CSP_AC, SEXE_AC, ANNAIS_AC, SITMAT_AC, QUALITE_VE, CSP_VE, SEXE_VE, ANNAIS_VE, SITMAT_VE, NUMCOM_AC, PADEPT_AC, CODNAT_AC

- Niveau le plus fin d'agrégation  en sortie : carreaux 200m

#### Champ d'investigation sur les régimes d'achat et de mutation des biens

- Source : échantillon BIEN ; Données PTZ

- Information pincipale : Part des achats à crédit, Loan-To-Value montant des crédits, nature des crédits, dispositifs d'accession à la propriété), conditions de la mutation selon les caractéristiques de la vente par le profil du vendeur (Turn-over ; nature de l'acquistion du bien par le vendeur)

- Types de données de sortie : variables quantitatives continues

- Variables d'origine BIEN : REQ_PRIX, MTCREDIT, PRESCREDIT, ANNAIS_VE, TYPMUTPREC, DATMUTPREC

- Variables d'origine PTZ : tope, napp, tysu

- Niveau le plus fin d'agrégation  en sortie : carreaux 200m pour données BIEN ; communes pour données PTZ

#### Champ d'investigation sur les types de biens 

- Source : échantillon BIEN 

- Information pincipale : profil des logements vendus/achetés sur le marché

- Types de données de sortie : variables quantitatives continues (exprimées en %)

- Variables d'origine : REQTYPBIEN, NBRPIECE, TYPAP, TYPMAI, REQ_EPOQU, REQ_ANC

- Niveau le plus fin d'agrégation  en sortie : carreaux 200m


## Livrable par références sur les groupes sociaux

Le livrable par références sur les groupes sociaux se déclinent en trois entrées. La première entrée est relative à une déclinaison des groupes en catégories socio-professionnelles. La seconde entrée est relative à une dimension démographique et les groupes se déclinent en tranche d'âges. La troisième entrée est relative à une déclinaison des groupes par genre. 

Les indicateurs produits répondent à trois grands thèmes : les inégalités socio-économiques; les réseaux sociaux du marché et l'évolution de l'accès au marché. 

#### Champ d'investigation sur les prix immobiliers

- Source : échantillon BIEN 

- Informations pincipales : Prix nominal net vendeur, prix au $m^2$

- Types de données de sortie : variables quantitatives continues

- Variables d'origine : REQ_PRIX, REQ_PM2, REQTYPBIEN, NBPIECES

- Niveau le plus fin d'agrégation  en sortie : Groupes de population (population régionale)

### Le champ d'investigation sur le profil des acquéreurs vendeurs.

- Source : échantillon BIEN

- Informations principales : profil des acquéreurs vendeurs, selon la CSP, l'âge et le sexe, la situation matrimoniale, le type de bien, les modalités d'accès à la propriété (acquisition, donation...),  la durée d'occupation du bien (pour les vendeurs), les réseaux sociaux d'acquéreurs vendeurs (relations d'achats et de ventes: qui achète à qui ; qui vend à qui)

- Types de données de sortie : variables quantitatives continues 

- Variables d'origine : QUALITE_VE, QUALITE_AC, CSP_AC, CSP_VE, ANNAIS_VE, ANNAIS_AC, SEXE_AC, SEXE_VE, SIMAT_AC, SITMAT_VE, REQTYPBIEN, DATMUTPREC, TYPMUTPREC  

- Niveau le plus fin d'agrégation en sortie : Groupes de population (population régionale)


#### Champ d'investigation sur les régimes d'achat et mutation

- Source : BIEN et PTZ 

- Informations pincipales : Part des achats à crédit, Loan-To-Value, montant des crédits, nature des crédits, dispositifs d'accession à la propriété), conditions de la mutation selon les caractéristiques de la vente par le profil du vendeur (Turn-over ; nature de l'acquistion du bien par le vendeur), taux d'intérêts (TEG), Durée de remboursement du crédit, Montant des crédits aidés et libres, revenus des ménages acquéreurs avec PTZ 

- Types de données de sortie : variables quantitatives continues

- Variables d'origine BIEN : REQ_PRIX, MTCREDIT, PRESCREDIT, ANNAIS_VE, TYPMUTPREC, DATMUTPREC

- Variables d'origine PTZ : rann, rani, ccspq, tegp, dtpp, vtpz, vtto, vtpr

- Niveau le plus fin d'agrégation  en sortie : Groupes de population (population régionale)


#### Champ d'investigation sur les types de biens 

- Source : échantillon BIEN 

- Information pincipale : profil des logements vendus/achetés sur le marché

- Types de données de sortie : variables quantitatives continues (exprimées en %)

- Variables d'origine : REQTYPBIEN, NBRPIECE, TYPAP, TYPMAI, REQ_EPOQU, REQ_ANC

- Niveau le plus fin d'agrégation  en sortie : Groupes de population (population régionale)
