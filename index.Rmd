---
title: "Présentation du site internet"
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```


![](fig\logos.png)

Réalisé au sein de l'[ANR WIsDHoM](https://anr.fr/Projet-ANR-18-CE41-0004) par Thibault Le Corre (UMR 8504 Géographie-Cités), avec la contribution de Ronan Ysebaert (UMS 2414 RIATE), Timothée Giraud (UMS 2414 RIATE), Jean-Baptiste Durand (UMR 7300 Espaces) et Pierre Le Brun (UMR 7300 Espaces), le projet CASSMIR (Contribution à l'Analyse Spatiale et Sociale des Marchés Immobiliers Résidentiels) poursuit l'objectif de produire un jeu de données sur le marché du logement à la propriété en Île-de-France, prêt à l'emploi, diffusable et réutilisable par la communauté scientifique. 

![](fig\DensityTransacsBIENMap.png)

## Le site internet CASSMIR 

Ce site internet est spécifiquement dédié au projet CASSMIR :

- Il renvoie à l'ensemble des données produites au cours du projet. Ces données sont entreposées sur Zenodo. **Les données produites et les fichiers de métadonnées associés sont accessibles en suivant ce [lien](https://zenodo.org/record/4030699#.X2M3MIs682x)**. Les données du projet sont diffusées sous **Creative Commons Attribution Non Commercial Share Alike 4.0 International (CC-BY-NC)**. Chaque utilisateur est invité à prendre connaissance des termes légaux de la [licence](https://creativecommons.org/licenses/by-nc-sa/4.0/).

- Il restitue l'ingénérie qui assure la repoductibilité du travail de préparation des données brutes vers la base de données CASSMIR. L'ensemble des codes du programme informatique R sont mis à disposition sur ce site. Ils sont par ailleurs disponible sur la plateforme de partage Git Lab en suivant ce [lien](https://gitlab.huma-num.fr/tlecorre/cassmir/-/tree/master). 
![](fig/fun.jpeg).

- La BD CASSMIR est amenée à être enrichie par l'import de nouvelles données. Le site internet sera régulièrement actualisé en tenant compte des mises à jour associées à de nouvelles versions de la base CASSMIR. Le projet se veut également participatif au sein de la communauté scientifique. Des propositions d'analyse thématiques à partir de la BD CASSMIR sont exemplifiées par l'équipe de l'ANR WIsDHoM. Nous invitons les personnes extérieures à cette équipe de participer au projet en partageant sur la plateforme Git Lab des codes reproductibles en langage R, que cela soit pour de simples analyses exploratoires de curiosité, ou que cela soit dans le cadre d'une publication scientifique avec des traitements qui mobilisent la BD CASSMIR. Ces différentes analyses seront alors publiées sur le site internet, en mentionnant le contributeur et en renvoyant à la production scientifique de l'auteur. 


## Versionnage

Cette rubrique renseigne les versions du projet et détaille les modifications qui interviennent au cours du projet ainsi que l'objet d'étude, et le recouvrement spatial et temporel des données harmonisées.

Il s'agit ici de la seconde version (V.2.0.0) du projet CASSMIR. Les données collectées pour la production de la base CASSMIR proviennent de deux bases de données d'origine : la base BIEN et la base PTZ. Pour les données issues de la base BIEN : les données appariées recouvrent la région Île-de-France sur une période allant de 1996 à 201,8 découpée en tranches annuelles : 1996, 1999, 2003 à 2012, 2015 et 2018. Pour les données issues de la base PTZ : les données appariées recouvrent la région Île-de-France sur une période allant de 1996 à 2016, découpée en tranches annuelles continues de 1996 à 2016.

Le passage entre la V 1.0.0 à la V 2.0.0 résulte de l'actualisation de la base avec l'appariement des données 2015 et 2018 issues de la base BIEN. Par ailleurs, un protocole de vérification et d'amélioration des localisations des transactions individuelles à partir des parcelles cadastrales d'appartenance a été mis en placelors de ce second versionnage.

## Structure du site

Le site est organisé de la façon suivante :

- L'onglet "Le projet et les sources" présente en détail le projet et les sources de données utilisées à la constitution de la base CASSMIR.

- L'onglet "Génération d'échantillons de données individuelles" présente la démarche de reproductibilité du travail en mettant à disposition deux échantillons de données anonymisés et randomisés issus de la base BIEN et PTZ et qui sont spatialisés dans un espace fictif. Avec ces échantillons, chaque utilisateur peut prendre connaissance des données brutes utilisées dans CASSMIR.

- L'onglet "Préparation de la base CASSMIR à partir des échantillons" propose les scripts et les explications qui permettent de reproduire les opérations menent à la production de la base de données CASSMIR à partir des deux échantillons des données sources mis à disposition. 

- L'onglet "Préparation des données BIEN" abrite les codes sources qui permettent de reproduire intégralement les opérations sur les données BIEN. Chaque utilisateur en possession d'un jeu de données identique peut rejouer la totalité des opérations qui mènent à la production des indicateurs harmonisés. Certaines étapes, n'étant pas entièrement exécutables à partir du jeu de données anonymisées, sont explicitées. 

- L'onglet "Préparation des données PTZ" abrite, de la même manière que l'onglet précédent, les codes sources qui permettent de reproduire intégralement les opérations sur les données PTZ. Contrairement à la base BIEN, les données PTZ sont partiellement disponibles en Open Access, avec certaines informations qui en sont amputées et qui sont mobilisées dans ce travail.

- L'onglet "Droits d'utilisation et description des variables" expose les métadonnées des fichiers de données entreposés sur Zenodo.

- L'onglet "Exploration de la base de données CASSMIR" expose le protocole pour la préparation des différentes couches géométriques modèles utilisées pour des représentations cartographiques (templates) et livre des exemples d'analyse à partir des données de la base CASSMIR. 


## Accès aux données

Cette rubrique résume les différentes conditions et modalités d'accès aux données du projet.

- Données sources utilisées pour la construction des indicateurs harmonisés sur le marché immobilier (BIEN et PTZ) : accès restreint à l'établissement d'une convention avec les services en charges de la diffusion des données. 

- Données sources utilisées pour la construction des couches géométriques modèles : libre accès auprès des services producteurs. 

- Autres données sources utilisées : libre accès auprès des services producteurs.

- Données appariées correspondant à la base CASSMIR : libre acccès dans le respect de la licence Creative Commons Attribution Non Commercial Share Alike 4.0 International (CC-BY-NC).
